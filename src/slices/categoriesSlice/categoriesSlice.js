import { createAsyncThunk } from "@reduxjs/toolkit";

export const fetchCategoriesRedux = createAsyncThunk(
    "categories/fetchCategories",
    async () => {
        const response = await fetch(
            "https://rapidapi.p.rapidapi.com/list.php?c=list",
            {
                method: "GET",
                headers: {
                    "x-rapidapi-host": "the-cocktail-db.p.rapidapi.com",
                    "x-rapidapi-key":
                        "7b9200bb94msheede853aada6416p1bf623jsn1fd6ea76330d",
                },
            }
        )
            .then((res) => res.json())
            .then((result) => {
                return result.drinks;
            })
            .catch((err) => {
                console.error(err);
            });
        return response;
    }

);
