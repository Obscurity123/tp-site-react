import { createAsyncThunk } from "@reduxjs/toolkit";

export const fetchDrinksByCategoryRedux = createAsyncThunk(
    "drinks/fetchDrinksByCategory",
    async (category) => {
        const response = await fetch(
            "https://rapidapi.p.rapidapi.com/filter.php?c=" + category,
            {
                method: "GET",
                headers: {
                    "x-rapidapi-host": "the-cocktail-db.p.rapidapi.com",
                    "x-rapidapi-key":
                        "7b9200bb94msheede853aada6416p1bf623jsn1fd6ea76330d",
                },
            }
        )
            .then((res) => res.json())
            .then((result) => {
                return result.drinks;
            })
            .catch((err) => {
                console.error(err);
            });
        return response;
    }
);

export const fetchDrinkByIdRedux = createAsyncThunk(
    "drinks/fetchDrinkById",
    async (id) => {
        const response = await fetch(
            "https://the-cocktail-db.p.rapidapi.com/lookup.php?i=" + id,
            {
                method: "GET",
                headers: {
                    "x-rapidapi-host": "the-cocktail-db.p.rapidapi.com",
                    "x-rapidapi-key":
                        "7b9200bb94msheede853aada6416p1bf623jsn1fd6ea76330d",
                    "Access-Control-Allow-Origin": "crossorigin",
                },
            }
        )
            .then((res) => res.json())
            .then((result) => {
                return result.drinks;
            })
            .catch((err) => {
                console.error(err);
            });
        return response;
    }
);

export const fetchRandomDrinksRedux = createAsyncThunk(
    "drinks/fetchRandomDrinks",
    async () => {
        const response = await fetch("https://rapidapi.p.rapidapi.com/randomselection.php", {
            method: "GET",
            headers: {
                "x-rapidapi-host": "the-cocktail-db.p.rapidapi.com",
                "x-rapidapi-key":
                    "7b9200bb94msheede853aada6416p1bf623jsn1fd6ea76330d",
                "Access-Control-Allow-Origin": "crossorigin",
            },
        })
            .then((res) => res.json())
            .then(
                (result) => {
                    return result.drinks;
                },
            )
            .catch((err) => {
                console.error(err);
            });
        return response;
    }
);
