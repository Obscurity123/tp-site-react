import React, { useState, useEffect } from "react";
import Navbar from "../../features/Navbar/Navbar";
import CocktailsRandom from "../../features/CocktailsRandom/CocktailsRandom";
import Footer from "../../features/Footer/Footer";
import { fetchRandomDrinksRedux } from "../../slices/drinksSlice/drinksSlice";
import { useDispatch } from "react-redux";

import "../../common.css";

import cocktailLogo from "../../assets/giphy.gif";

const Home = () => {

    // Set Initial States
    
    const dispatch = useDispatch();
    const [drinks, setDrinks] = useState([]);

    useEffect(() => {

        let drinksRedux = dispatch(fetchRandomDrinksRedux());

        Promise.resolve(drinksRedux).then((res) =>
            setDrinks(res.payload)
        );

    }, []);

    if (drinks.length !== 0) {
        return (
            <div>
                <Navbar />
                <div className="text-center mt-4">
                    <h2>Random cocktails</h2>
                    <CocktailsRandom drinks={drinks} />
                </div>
                <Footer />
            </div>
        );
    } else {
        return (
            <div>
                <Navbar/>
                <div className="customContainerLoading">
                    <h2 className="pb-3">Datas is loading, please wait...</h2>
                    <img src={cocktailLogo} alt="logo" height="400" width="400"/>
                </div>
                <Footer />
            </div>
        );
    }
};

export default Home;
