import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import { fetchDrinkByIdRedux } from "../../slices/drinksSlice/drinksSlice";
import { useDispatch } from "react-redux";

const CardComponent = ({ drink }) => {

    // Set Initial States

    const dispatch = useDispatch();
    const [drinkDisplay, setDrinkDisplay] = useState(drink);

    useEffect(() => {

        let drinksRedux = dispatch(fetchDrinkByIdRedux(drink.idDrink));

        Promise.resolve(drinksRedux).then((res) => {
            setDrinkDisplay(res.payload[0]);
        });

    }, [drink]);

    return (
        <div className="card col-10 col-md-4 col-lg-3 ml-md-3 mb-5">
            <img
                className="card-img-top mt-2"
                src={drinkDisplay.strDrinkThumb}
                height="200px"
                alt="Card image cap"
            />
            <div className="card-body">
                <h5 className="card-title">{drinkDisplay.strDrink}</h5>
                <p className="card-text">
                    Category : {drinkDisplay.strCategory}
                </p>
                <p className="card-text">Type : {drinkDisplay.strAlcoholic}</p>
                <p className="card-text">
                    Type of glass : {drinkDisplay.strGlass}
                </p>
                <div className="text-center">
                    <Link
                        to={"/cocktail/" + drinkDisplay.idDrink}
                        className="btn btn-primary"
                    >
                        Show recipe
                    </Link>
                </div>
            </div>
        </div>
    );
};

export default CardComponent;