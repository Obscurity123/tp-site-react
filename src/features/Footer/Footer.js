import React from "react";
import "./Footer.css";

const Footer = () => {

    return(

        <div id="footer" className="bg-dark text-white text-center pb-3 pt-3">
            <p className="mb-0">© Created by Cocktails Company</p>
        </div>

    )

}

export default Footer; 