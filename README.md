### Run npm i for dl nodes_modules

### Architecture Folders

- public
- src
    - app
        - store redux
    - features (components)
        - CardComponent
        - CardDetailsComponent
        - CocktailsRandom
        - Footer
        - FormSelectCategory
        - Navbar
        - PaginationFilterByCategory
    - pages
        - Cocktail
        - CocktailsByCategory
        - Home
    - slices redux
        - categoriesSlice request API
        - drinksSlice request API

### Architecture Web

## Pages

- Home page (Random cocktails) path : "/"
- CocktailsByCategory page path : "/byCategory"
- Cocktail page path : "/cocktail/:id"

## Link between pages and components

# Home page

- Navbar
- CocktailsRandom
    - CardComponent
- Footer

# CocktailsByCategory

- Navbar
- FormSelectCategory
- CardComponent
- PaginationFilterByCategory
- Footer

# Cocktail

- Navbar
- CardDetailsComponent
- Footer